#include "contactsmodel.h"

ContactsModel::ContactsModel(QObject* parent) : QAbstractTableModel(parent){
}

int ContactsModel::rowCount(const QModelIndex& parent) const{
    Q_UNUSED(parent)
    return publications.count();
}

int ContactsModel::columnCount(const QModelIndex& parent) const{
    Q_UNUSED(parent)
    return LAST;
}

QVariant ContactsModel::data(const QModelIndex& index, int role) const{
    if(!index.isValid() || publications.count() <= index.row() ||
            (role != Qt::DisplayRole && role != Qt::EditRole)){
        return QVariant();
    }

    return publications[index.row()][Column(index.column())];
}

bool ContactsModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    if(!index.isValid() || role != Qt::EditRole || publications.count() <= index.row()){
        return false;
    }

    publications[index.row()][Column(index.column())] = value;
    emit dataChanged(index, index);
    return true;
}

QVariant ContactsModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(role != Qt::DisplayRole){
        return QVariant();
    }

    if(orientation == Qt::Vertical){
        return section;
    }

    switch(section){
        case NAME:
            return trUtf8("NAME");
        case AUTHOR:
            return trUtf8("AUTHOR");
    }

    return QVariant();
}

void ContactsModel::addPublication(ContactBase* base){
    PublicationData publication;

        for(int i = 0; i < base->getList().size(); i++){
            publication[NAME] = base->getList()[i].name;
            publication[AUTHOR] = base->getList()[i].phone;
            int row = publications.count();
            beginInsertRows(QModelIndex(), row, row);
            publications.append(publication);
            endInsertRows();
        }
}

void ContactsModel::clear(){
    publications.erase(publications.begin(), publications.end());
}
