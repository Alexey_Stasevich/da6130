#ifndef PUBLICMODEL_H
#define PUBLICMODEL_H

#include <contactbase.h>
#include <QAbstractTableModel>

class ContactsModel : public QAbstractTableModel{

    Q_OBJECT

private:
    enum Column {
        NAME = 0,
        AUTHOR,
        LAST
    };

    typedef QHash<Column, QVariant> PublicationData;
    typedef QList<PublicationData> Publications;
    Publications publications;

public:
    ContactsModel(QObject* parent = 0);

    bool setData(const QModelIndex& index, const QVariant& value, int role);
    int rowCount (const QModelIndex& parent) const;
    int columnCount (const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void clear();
    void addPublication(ContactBase*);
    //SEARCH

};

#endif // PUBLICMODEL_H
