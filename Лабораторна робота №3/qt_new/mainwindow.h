#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "contactsmodel.h"

#include <QTableView>
#include <QWidget>
#include <QHeaderView>
#include <QLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>

#include "contact.h"

class MainWindow : public QWidget {

    Q_OBJECT

private:
    QTableView* view;
    ContactsModel* model;
    ContactBase* base;
    ContactBase* find;
    QLineEdit* name;
    QLineEdit* phone;

public:
    MainWindow(QWidget *parent = 0);
    void setBase(QList<Contact>);
    void genCutted(ContactBase*, const QString&);
    void genBase();

    ~MainWindow();

private slots:
    void generate();
    void search();

};

#endif // MAINWINDOW_H
