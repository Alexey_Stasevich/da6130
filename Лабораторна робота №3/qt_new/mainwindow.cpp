#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :QWidget(parent){
    QVBoxLayout* mainLayout = new QVBoxLayout(0);
    setLayout(mainLayout);

    view = new QTableView;
    view->setModel(model = new ContactsModel);
    view->verticalHeader()->setVisible(false);

    view->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);


    mainLayout->addWidget(view);

        QGridLayout* panelLayout = new QGridLayout;
        mainLayout->addLayout(panelLayout);


        QPushButton* buttonStr = new QPushButton(trUtf8("STAR WORK"));
        connect(buttonStr, SIGNAL(clicked(bool)), SLOT(generate()));
        panelLayout->addWidget(buttonStr, 0, 0);

        QPushButton* buttonSrc = new QPushButton(trUtf8("SEARCH"));
        connect(buttonSrc, SIGNAL(clicked(bool)), SLOT(search()));
        panelLayout->addWidget(buttonSrc, 1, 0);

            name = new QLineEdit;
            panelLayout->addWidget(name, 2, 0);

        setStyleSheet("background-color:#282828;\n color:#ffffff");

        resize(800, 600);
}

MainWindow::~MainWindow(){
}

void MainWindow::generate(){
            delete model;
            view->setModel(model = new ContactsModel);

        model->addPublication(base);
}

void MainWindow::search(){
        genCutted(base, name->text());

            delete model;
            view->setModel(model = new ContactsModel);

        model->addPublication(find);
}

void MainWindow::genBase(){
    base = new ContactBase();
    base->setPhone(Contact("Andrey", "+123456789"));
    base->setPhone(Contact("Andrey", "+313133131"));
    base->setPhone(Contact("Alexey", "+437173717"));
    base->setPhone(Contact("Sergey", "+213328832"));
    base->setPhone(Contact("Vladislav", "+099113133"));
    base->setPhone(Contact("Alexandr", "+808108301"));
}

void MainWindow::genCutted(ContactBase* base, const QString& name){
    QList<Contact> temp;
    QList<Contact> list = base->getList();

    if(!list.empty()){
        for(int i = 0; i < list.size(); i++){
            if(list[i].name == name){
                temp.push_back(list[i]);
            }
        }

        find = new ContactBase();
        find->setList(temp);
    }
}
