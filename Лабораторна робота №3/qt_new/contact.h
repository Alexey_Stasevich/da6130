#ifndef CONTRACT_H
#define CONTRACT_H

#include <QString>

struct Contact{
    QString name;
    QString phone;
    Contact(const QString&, const QString&);

    bool operator == (const Contact& target);
};

#endif // CONTRACT_H
