#include "mainwindow.h"
#include <QApplication>

#include "contactbase.h"

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    MainWindow* window = new MainWindow(nullptr);

    window->genBase();
    window->show();

    return application.exec();
}
