#ifndef PHONEBASE_H
#define PHONEBASE_H

#include "contact.h"
#include <QList>

class ContactBase {
private:
    QList<Contact> base;

public:
    ContactBase();

    void setPhone(Contact);
    QList<Contact> getList();
    void setList(QList<Contact>);
};

#endif // PHONEBASE_H
