#include "stdafx.h"
#include "Contact.h"
#include <string>
#include <iostream>
#include <algorithm>
std::string Names[] = { "Andrey", "Anton", "Aleksey", "Boris", "Vladislav", "Rikimaru", "Filipp", "Inokentii" };
std::string Surnames[] = { "Flakonov", "Hiranskii", "Defalo", "Cachinskii", "Srul", "Pudgovii", "Chumak", "Rtutnii" };

Contact::Contact()
{
	Name = Names[rand() % 8];
	Surname = Surnames[rand() % 10];
	TelephoneNumber = rand() % 1000000000;
	Mail = Name+"."+Surname+"@gmail.com";
	Skype = Name + Surname;
}

std::string Contact::getName()
{
	return Name;
}

std::string Contact::getSurname()
{
	return Surname;
}

int Contact::getTelephoneNumber()
{
	return TelephoneNumber;
}

std::string Contact::getMail()
{
	return Mail;
}

std::string Contact::getSkype()
{
	return Skype;
}


bool Contact::Comparing(const Contact &a, const Contact &b) 
{
	return (a.TelephoneNumber > b.TelephoneNumber);
}