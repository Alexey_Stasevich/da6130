#include "stdafx.h"
#include "View.h"
#include <string> 
#include <iostream>
#include "Contact.h"
#include "Ierarchy.h"
#include <vector>

View::View()
{
}


void View::ShowIerarchy(Ierarchy A1) {
	Contact specContact;
	for (int i = 0; i <= 8; i++)
	{
		specContact = (A1.Association->at(i));
		this->ShowContact(specContact);
	};
}

void View::ShowContact(Contact A1) {
	std::cout << " Name - " << A1.getName() << std::endl;
	std::cout << " Surname - " << A1.getSurname() << std::endl;
	std::cout << " Telephone number - " << A1.getTelephoneNumber() << std::endl;
	std::cout << " Mail - " << A1.getMail() << std::endl;
	std::cout << " Skype - " << A1.getSkype() << std::endl; 
}