#pragma once
#include <string>
#include <set>
class Contact
{
public:
	Contact();
	Contact(std::string name, std::string surname, int telephonenumber, std::string mail, std::string skype)
		:Name(name), Surname(surname), TelephoneNumber(telephonenumber), Mail(mail), Skype(skype)
	{};
	bool Contact::Comparing(const Contact& a, const Contact& b);
	std::string getName();
	std::string getSurname();
	int getTelephoneNumber();
	std::string getMail();
	std::string getSkype();
	int TelephoneNumber;
private:
	std::string Name;
	std::string Surname;
	std::string Mail;
	std::string Skype;
	
};

