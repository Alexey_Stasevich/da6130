#pragma once

#include "Contact.h"
#include "Ierarchy.h"
#include <iostream>
#include <algorithm>
#include <vector>

class View
{
public:
	View();
	~View();
	void ShowIerarchy(Ierarchy A1);
	void ShowContact(Contact A1);
};

