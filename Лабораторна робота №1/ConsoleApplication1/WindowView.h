#pragma once
#include "WindowModel.h"

class WindowView
{
private:
	WindowModel Model;

public:
	void show(const WindowModel& window);
	int action();
	void answer(int p);
};

