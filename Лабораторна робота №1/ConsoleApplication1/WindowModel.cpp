#include "stdafx.h"
#include "WindowModel.h"


WindowModel::WindowModel()
{
	WindowModel::height = 1.0;
	WindowModel::width = 0.5;
	WindowModel::frame_material = "wood";
	WindowModel::integrity=true;
	WindowModel::dustiness=true;
	WindowModel::close=true;
}

bool WindowModel::checkintegrity()
{
	return integrity;
}

bool WindowModel::checkdustiness()
{
	return dustiness;
}

bool WindowModel::closeORnot()
{
	return close;
}

WindowModel::~WindowModel()
{
}

double WindowModel::GetWindowHeight()
{
	return height;
}

double WindowModel::GetWindowWidth()
{
	return width;
}

std::string WindowModel::GetWindowFrameMaterial()
{
	return frame_material;
}