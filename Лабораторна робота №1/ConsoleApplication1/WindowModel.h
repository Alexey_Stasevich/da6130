#pragma once
#include <string>

class WindowModel
{
public:
	WindowModel();
	~WindowModel();
	double GetWindowHeight();
	double GetWindowWidth();
	std::string GetWindowFrameMaterial();
	bool checkintegrity();
	bool checkdustiness();
	bool closeORnot();

private:
	double height;
	double width;
    std::string frame_material;
	bool integrity;
	bool dustiness;
	bool close;
};

























