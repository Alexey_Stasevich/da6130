#include "stdafx.h"
#include "WindowController.h"


WindowController::WindowController(WindowModel& Model, WindowView& View)
{
	WindowController::Model = Model;
	WindowController::View = View;
}

WindowController::~WindowController()
{

}

void WindowController::showMenu()
{
	View.show(Model);
	View.action();
}

int WindowController::doAction()
{
	bool c = Model.closeORnot();
	bool i = Model.checkintegrity();
	bool d = Model.checkdustiness();
	while (1)
	{
		switch (View.action())
		{
		case 1:
		{
			if (c)
			{
				View.answer(0);
				c = false;
			}
			else
			{
				View.answer(1);
				c = false;
			}
		}
		case 2:
		{
			if (c)
			{
				if (d)
					View.answer(2);
				else
					View.answer(3);
			}
			else
				View.answer(4);
		}
		case 3:
		{
			if (i)
			{
				i = false;
				d = false;
				View.answer(5);
			}
			else
				View.answer(6);
		}
		case 4:
		{
			d = false;
			View.answer(7);
        }
		case 0:
		{
			exit(0);
        }
		default:
		{
			View.answer(8);
		}
		}
	}
}