#include "stdafx.h"
#include "WindowView.h"
#include "WindowModel.h"
#include <iostream>


void WindowView::show(const WindowModel& window)
{
	std::cout << std::endl;
	std::cout << "Hello! This program is giving you control for window." << std::endl;
	std::cout << "Here you can see windows properties:" << std::endl;
	std::cout << "Height - " << Model.GetWindowHeight() << " meters\n" << std::endl;
	std::cout << "Long - " << Model.GetWindowWidth() << " meters\n" << std::endl;
	std::cout << "Frame material - " << Model.GetWindowFrameMaterial() << std::endl;
	std::cout << "What do you want to do with window?" << std::endl;
	std::cout << "Press 1 to open the window." << std::endl;
	std::cout << "Press 2 to look through the window." << std::endl;
	std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
	std::cout << "Press 4 to clean the window." << std::endl;
	std::cout << "For ending the program press 0." << std::endl;
	std::cout << std::endl;
}

int WindowView::action()
{
	int a;
	std::cin >> a;
	return a;
}

void WindowView::answer(int p)
{
	switch (p)
	{
	case 0: 
		{
			std::cout << "Ok. What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to close the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
		}
	case 1:
	    {
			std::cout << "Ok. What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	case 2:
	    {
			std::cout << "The window is too dirty. You can't actually see anything." << std::endl;
			std::cout << "What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	case 3:
	    {
			std::cout << "What a great day it is. Maybe you will go for a walk?" << std::endl;
			std::cout << "What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	case 4:
	    {
			std::cout << "What a great day it is. Maybe, you will go for a walk?" << std::endl;
			std::cout << "What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to close the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	case 5:
     	{
			std::cout << "You broke the window. Why did you do this?!" << std::endl;
			std::cout << "What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to call master for removing the window." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	case 6:
	    {
			std::cout << "The window is already broken. Your job here is done. Maybe, you should call a master?" << std::endl;
			std::cout << "What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to call master for removing the window." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	case 7:
	    {
			std::cout << "You clean the window. Now it's clean." << std::endl;
			std::cout << "What else do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	default:
	    {
			std::cout << "Wrong action! Try again." << std::endl;
			std::cout << "What do you want to do with window?" << std::endl;
			std::cout << "Press 1 to open the window." << std::endl;
			std::cout << "Press 2 to look through the window." << std::endl;
			std::cout << "Press 3 to break the window(why?:c...)." << std::endl;
			std::cout << "Press 4 to clean the window." << std::endl;
			std::cout << "For ending the program press 0." << std::endl;
			std::cout << std::endl;
	    }
	}

}



