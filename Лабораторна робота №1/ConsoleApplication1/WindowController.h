#pragma once

#include "WindowModel.h"
#include "WindowView.h"

class WindowController
{
public:
	WindowController(WindowModel&, WindowView&);
	~WindowController();
	void showMenu();
	int doAction();

private:
	WindowModel Model;
	WindowView View;

};

